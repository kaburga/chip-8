with SDL;
with SDL.Timers;
with SDL.Video.Pixel_Formats;
with SDL.Video.Surfaces;
with SDL.Video.Windows;
with SDL.Video.Windows.Makers;
with SDL.Events.Events;
with SDL.Events.Keyboards;
with SDL.Video.Renderers;
with SDL.Video.Renderers.Makers;
with SDL.Video.Rectangles;
with SDL.Video.Palettes;
with Interfaces.C;
with Chip8.Data;
with Ada.Float_Text_IO;
with Ada.Text_IO;

package body Chip8.Human_Machine_Interaction is
   use type SDL.Events.Event_Types;
   use type SDL.Events.Keyboards.Key_Codes;
   use type SDL.Events.Keyboards.Scan_Codes;
   use SDL.Events.Keyboards;
   use SDL.Timers;
   use SDL.Video.Renderers;
   use Data;

   Pixel_Size            : constant := 10;
   Width                 : constant := (Screen_Width'Last  + 1) * Pixel_Size; --640
   Height                : constant := (Screen_Height'Last + 1) * Pixel_Size; --320
   Screen_FPS            : constant Milliseconds := Data.Frames_Per_Second;

   Window   : SDL.Video.Windows.Window;
   Renderer : SDL.Video.Renderers.Renderer;
   Event    : SDL.Events.Events.Events;

   Counted_Frames : Float;
   Start_Tick     : Milliseconds;

   Input_Keys : constant array(0 .. 15) of SDL.Events.Keyboards.Key_Codes :=
     (Code_1, Code_2, Code_3, Code_4,  -- 1 2 3 C
      Code_Q, Code_W, Code_E, Code_R,  -- 4 5 6 D
      Code_A, Code_S, Code_D, Code_F,  -- 7 8 9 E
      Code_Z, Code_X, Code_C, Code_V); -- A 0 B F

   Key_Map : constant array(Input_Keys_Index_Type) of Natural :=
     (16#01#, 16#02#, 16#03#, 16#0C#,
      16#04#, 16#05#, 16#06#, 16#0D#,
      16#07#, 16#08#, 16#09#, 16#0E#,
      16#0A#, 16#00#, 16#0B#, 16#0F#);

   procedure Display_Init is
   begin
      if not SDL.Initialise then
         Chip8_Error;
         return;
      end if;
      SDL.Video.Windows.Makers.Create (Win      => Window,
                                       Title    => "Chip-8 Emulator",
                                       Position => SDL.Natural_Coordinates'(X => 0, Y => 0),
                                       Size     => SDL.Positive_Sizes'(Width, Height));
      SDL.Video.Renderers.Makers.Create (Rend   => Renderer,
                                         Window => Window,
                                         Flags  => Accelerated or Present_V_Sync);
      Start_Tick := SDL.Timers.Ticks;
      Counted_Frames := 1.0;
   end Display_Init;

   procedure Display_Update is
      function Get_Ticks(Timestamp: in Milliseconds) return Milliseconds is
      begin
         return SDL.Timers.Ticks - Timestamp;
      end Get_Ticks;

      Rectangle : SDL.Video.Rectangles.Rectangle;
   begin
      Renderer.Set_Draw_Colour(SDL.Video.Palettes.Colour'(0, 0, 0, 255));
      Renderer.Clear;
      for X in Screen_Width loop
         for Y in Screen_Height loop
            if Display_Area(X, Y) = True then
               Rectangle := (X      => Interfaces.C.Int(X * Pixel_Size),
                             Y      => Interfaces.C.Int(Y * Pixel_Size),
                             Width  => Pixel_Size,
                             Height => Pixel_Size);
               Renderer.Set_Draw_Colour(SDL.Video.Palettes.Colour'(255, 255, 255, 255));
               Renderer.Fill(Rectangle);
            end if;
         end loop;
      end loop;
      Renderer.Present;

      declare
         Average_FPS : Float := (Counted_Frames / (Float(Get_Ticks(Start_Tick)) / 1000.0));
      begin
         if (Natural(Counted_Frames) mod 120) = 0 then
            Ada.Text_IO.Put("Average FPS: ");
            Ada.Float_Text_IO.Put(Average_Fps,
                                  Fore => 2,
                                  Aft  => 1,
                                  Exp  => 0);
            Ada.Text_Io.New_Line;
         end if;
         Counted_Frames := Counted_Frames + 1.0;
      end;
   end Display_Update;

   procedure Display_Clear is
   begin
      Display_Area := (others =>(others => False));
   end Display_Clear;

   procedure Display_Close is
   begin
      Window.Finalize;
      SDL.Finalise;
   end Display_Close;

   procedure Input_Keys_Status_Update is
   begin
      while SDL.Events.Events.Poll(Event) loop
         case Event.Common.Event_Type is
            when SDL.Events.Quit =>
               Halt := True;

            when SDL.Events.Keyboards.Key_Down =>
               if Event.Keyboard.Key_Sym.Key_Code = SDL.Events.Keyboards.Code_Escape then
                  Halt := True;
               end if;

               for Index in Input_Keys'Range loop
                  if Event.Keyboard.Key_Sym.Key_Code = Input_Keys(Index) then
                     Set_Key(Key_Map(Index),
                             True);
                  end if;
               end loop;

            when SDL.Events.Keyboards.Key_Up =>
               for Index in Input_Keys'Range loop
                  if Event.Keyboard.Key_Sym.Key_Code = Input_Keys(Index) then
                     Set_Key(Key_Map(Index),
                             False);
                  end if;
               end loop;

            when others =>
               null;
         end case;
      end loop;
   end Input_Keys_Status_Update;

end Chip8.Human_Machine_Interaction;
