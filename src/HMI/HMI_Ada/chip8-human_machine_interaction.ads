private package Chip8.Human_Machine_Interaction is

   procedure Display_Init;
   procedure Display_Update;
   procedure Display_Clear;
   procedure Display_Close;
   procedure Input_Keys_Status_Update;

end Chip8.Human_Machine_Interaction;
