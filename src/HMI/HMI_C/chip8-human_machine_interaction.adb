with Chip8.Data;   use Chip8.Data;
with Interfaces.C; use Interfaces.C;

package body Chip8.Human_Machine_Interaction is

   c_display_area : array(unsigned(0) .. unsigned(Screen_Width'Last),
                          unsigned(0) .. unsigned(Screen_Height'Last)) of unsigned := (others =>(others => unsigned(0)))
     with
       Export     => True,
       Convention => C;

   c_input_keys : array(Unsigned(0) .. Unsigned(15)) of unsigned := (others => 0)
     with
       Export     => True,
       Convention => C;

   c_halt : unsigned := 0
     with
       Export     => True,
       Convention => C;

   c_frames_per_second : unsigned := unsigned(Data.Frames_Per_Second)
     with
       Export     => True,
       Convention => C;

   procedure Display_Init is
      Success : Natural := 0;
   begin
      Success := C_Display_Init;
      if Success = 0 then
         Data.Halt := True;
      end if;
   end Display_Init;

   procedure Display_Clear is
   begin
      Display_Area := (others =>(others => False));
      c_display_area := (others =>(others => unsigned(0)));
   end Display_Clear;

   function Boolean_To_Unsigned(Input_Bool : in Boolean) return unsigned is
   begin
      if Input_Bool = True then
         return unsigned(1);
      else
         return unsigned(0);
      end if;
   end Boolean_To_Unsigned;

   function Unsigned_To_Boolean(Input_Unsigned : in unsigned) return Boolean is
   begin
      if Input_Unsigned = 1 then
         return True;
      else
         return False;
      end if;
   end Unsigned_To_Boolean;

   procedure Display_Update is
   begin
      for X in Screen_Width'Range loop
         for Y in Screen_Height'Range loop
            c_display_area(unsigned(X), unsigned(Y)) := Boolean_To_Unsigned(Display_Area(X, Y));
         end loop;
      end loop;
      C_Display_Update;
   end Display_Update;

   procedure Input_Keys_Status_Update is
   begin
      C_Input_Keys_Status_Update;
      for I in c_input_keys'Range loop
         Set_Key(Natural(I), Unsigned_To_Boolean(c_input_keys(I)));
      end loop;
      if c_halt = 1 then
         Halt := True;
      end if;
   end Input_Keys_Status_Update;

end Chip8.Human_Machine_Interaction;
