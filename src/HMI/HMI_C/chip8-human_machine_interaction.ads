package Chip8.Human_Machine_Interaction is

   procedure Display_Init;

   procedure Display_Clear;

   procedure Display_Update;

   procedure Display_Close;
   pragma Import (C, Display_Close, "display_close");

   procedure Input_Keys_Status_Update;

private
   function C_Display_Init return Integer;
   pragma Import (C, C_Display_Init, "display_init");

   procedure C_Display_Update;
   pragma Import (C, C_Display_Update, "display_update");

   procedure C_Input_Keys_Status_Update;
   pragma Import (C, C_Input_Keys_Status_Update, "input_keys_status_update");

end Chip8.Human_Machine_Interaction;
