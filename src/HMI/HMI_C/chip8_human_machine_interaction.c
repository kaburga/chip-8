#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "chip8_human_machine_interaction.h"

extern unsigned int c_frames_per_second;
extern unsigned int c_display_area[64][32];
extern unsigned int c_input_keys[16];
extern unsigned int c_halt;

const int SCREEN_WIDTH  = 640;
const int SCREEN_HEIGHT = 320;
const int SCREEN_FPS = 60;
const int SCREEN_TICK_PER_FRAME = 1000 / SCREEN_FPS;
const int pixelSize = 10;

uint8_t INPUT_KEYS[16] = {
    SDLK_1,
    SDLK_2,
    SDLK_3,
    SDLK_4,
    SDLK_q,
    SDLK_w,
    SDLK_e,
    SDLK_r,
    SDLK_a,
    SDLK_s,
    SDLK_d,
    SDLK_f,
    SDLK_z,
    SDLK_x,
    SDLK_c,
    SDLK_v,
};

uint8_t KEY_MAP[16] = {
    0x01,
    0x02,
    0x03,
    0x0c,
    0x04,
    0x05,
    0x06,
    0x0d,
    0x07,
    0x08,
    0x09,
    0x0e,
    0x0a,
    0x00,
    0x0b,
    0x0f,
};

int countedFrames = 1;
int startTick = 0;

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

bool display_init(void) {
    if (SCREEN_FPS != c_frames_per_second) {
        return false;
    }
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
        fprintf(stderr, "SDL_Init Error: %s\n", SDL_GetError());
        return false;
    }
    window = SDL_CreateWindow("Chip-8 Emulator",
                              SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED,
                              SCREEN_WIDTH,
                              SCREEN_HEIGHT,
                              SDL_WINDOW_SHOWN);
    if (window == NULL) {
        fprintf(stderr, "SDL_CreateWindow Error: %s\n", SDL_GetError());
        return false;
    }
    renderer = SDL_CreateRenderer(window,
                                  -1,
                                  SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL) {
        fprintf(stderr, "SDL_CreateRenderer Error: %s\n", SDL_GetError());
        return false;
    }

    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);

    startTick = SDL_GetTicks();

    return true;
}

void display_update(void) {
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
    SDL_RenderClear(renderer);

    for (int x = 0; x < 64; x++) {
        for (int y = 0; y < 32; y++) {
            if (c_display_area[x][y]) {
                SDL_Rect fillRect = {x*pixelSize, y*pixelSize, pixelSize, pixelSize};
                SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
                SDL_RenderFillRect(renderer, &fillRect);
            }
        }
    }
    SDL_RenderPresent(renderer);

    if (countedFrames % 120 == 0) {
        double avgFPS = countedFrames / ((SDL_GetTicks() - startTick) / 1000.);
        printf("Average FPS: %f\n", avgFPS);
    }
    ++countedFrames;
}

void display_close(void) {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    renderer = NULL;
    window = NULL;
    SDL_Quit();
}

void input_keys_status_update(void) {
    SDL_Event event;
    while (SDL_PollEvent(&event) != 0) {
        if (event.type == SDL_QUIT) {
            c_halt = 1;
        }
        if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_ESCAPE) {
                c_halt = 1;
            }
            for (int i = 0; i < sizeof(c_input_keys)/sizeof(unsigned int); i++) {
                if (event.key.keysym.sym == INPUT_KEYS[i])
                    c_input_keys[KEY_MAP[i]] = 1;
            }
        }
        if (event.type == SDL_KEYUP) {
            for (int i = 0; i < sizeof(c_input_keys)/sizeof(unsigned int); i++) {
                if (event.key.keysym.sym == INPUT_KEYS[i])
                    c_input_keys[KEY_MAP[i]] = 0;
            }
        }
    }
}
