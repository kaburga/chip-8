#ifndef _CHIP8_HUMAN_MACHINE_INTERACTION_H_
#define _CHIP8_HUMAN_MACHINE_INTERACTION_H_

bool display_init(void);
void display_update(void);
void display_close(void);
void input_keys_status_update(void);

#endif

