with Chip8.Data;

package body Chip8.Human_Machine_Interaction.Sound_Handler is

   procedure Sound_Init is
      Success : Natural := 0;
   begin
      Success := C_Sound_Init;
      if Success = 0 then
         Chip8_Error;
      end if;
   end Sound_Init;

end Chip8.Human_Machine_Interaction.Sound_Handler;
