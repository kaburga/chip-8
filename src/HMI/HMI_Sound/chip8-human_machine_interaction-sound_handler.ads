package Chip8.Human_Machine_Interaction.Sound_Handler is

   procedure Sound_Init;

   procedure Sound_Exit;
   pragma Import (C, Sound_Exit, "sound_exit");

   procedure Play_Soundeffect;
   pragma Import (C, Play_Soundeffect, "play_soundeffect");

private
   function C_Sound_Init return Integer;
   pragma Import (C, C_Sound_Init, "sound_init");
end Chip8.Human_Machine_Interaction.Sound_Handler;
