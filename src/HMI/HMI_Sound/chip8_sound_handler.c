#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

Mix_Chunk *gSound = NULL;

uint8_t sound_init(void) {
    if( SDL_Init(SDL_INIT_AUDIO) < 0 ) {
        fprintf(stderr, "SDL_Init Error: %s\n", SDL_GetError());
        return 0;
    }
    if(Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0) {
        printf( "SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError() );
        return 0;
    }
    gSound = Mix_LoadWAV( "src/HMI/HMI_Sound/sound_effect.wav" );
    if( gSound == NULL ) {
        printf("Failed to load sound. SDL_mixer Error: %s\n", Mix_GetError());
        return 0;
    }
    return 1;
}

void sound_exit(void) {
    gSound = NULL;
    Mix_Quit();
    return;
}

void play_soundeffect(void) {
    Mix_PlayChannel( -1, gSound, 0 );
    return;
}

