#ifndef _CHIP8_SOUND_HANDLER_H_
#define _CHIP8_SOUND_HANDLER_H_

uint8_t sound_init(void);
void sound_exit(void);
void play_soundeffect(void);

#endif

