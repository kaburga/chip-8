package body Chip8.Data is

   Font_Set : constant array(0 .. 15, 0 .. 4) of Byte :=
     ((16#F0#, 16#90#, 16#90#, 16#90#, 16#F0#),  -- 0
      (16#20#, 16#60#, 16#20#, 16#20#, 16#70#),  -- 1
      (16#F0#, 16#10#, 16#F0#, 16#80#, 16#F0#),  -- 2
      (16#F0#, 16#10#, 16#F0#, 16#10#, 16#F0#),  -- 3
      (16#90#, 16#90#, 16#F0#, 16#10#, 16#10#),  -- 4
      (16#F0#, 16#80#, 16#F0#, 16#10#, 16#F0#),  -- 5
      (16#F0#, 16#80#, 16#F0#, 16#90#, 16#F0#),  -- 6
      (16#F0#, 16#10#, 16#20#, 16#40#, 16#40#),  -- 7
      (16#F0#, 16#90#, 16#F0#, 16#90#, 16#F0#),  -- 8
      (16#F0#, 16#90#, 16#F0#, 16#10#, 16#F0#),  -- 9
      (16#F0#, 16#90#, 16#F0#, 16#90#, 16#90#),  -- A
      (16#E0#, 16#90#, 16#E0#, 16#90#, 16#E0#),  -- B
      (16#F0#, 16#80#, 16#80#, 16#80#, 16#F0#),  -- C
      (16#E0#, 16#90#, 16#90#, 16#90#, 16#E0#),  -- D
      (16#F0#, 16#80#, 16#F0#, 16#80#, 16#F0#),  -- E
      (16#F0#, 16#80#, 16#F0#, 16#80#, 16#80#)); -- F

   procedure Init is
   begin
      Opcode      := 0;
      V           := (others => 0);
      I           := 0;
      Sp          := 0;
      PC          := 16#200#;
      Stack       := (others => 0);
      Delay_Timer := 0;
      Sound_Timer := 0;
      Memory      := (others => 0);

      declare
         I : Natural := 0;
      begin
         for C of Font_Set loop
            Memory(I) := C;
            I := I + 1;
         end loop;
      end;

      Display_Area    := (others =>(others => False));
      Input_Keys      := (others => False);
      Halt            := True;
   end Init;

begin
   Init;
end Chip8.Data;
