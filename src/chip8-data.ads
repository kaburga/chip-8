private package Chip8.Data is
   pragma Elaborate_Body(Chip8.Data);

   Opcode      : Short;
   V           : Register_Type; -- general purpose register
   I           : Short;
   SP          : Stack_Index_Type;
   PC          : Memory_Index_Type;
   Stack       : Stack_Type;
   Delay_Timer : Byte;
   Sound_Timer : Byte;
   Memory      : Memory_Type;

   Display_Area : Display_Type;

   Input_Keys : Input_Keys_Type;

   Halt : Boolean;

   Frames_Per_Second            : constant Natural := 60;
   Number_Of_Opcodes_Per_Second : constant Natural := 480;

   procedure Init;

end Chip8.Data;
