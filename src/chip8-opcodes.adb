with Ada.Text_IO;
with Ada.Numerics.Discrete_Random;
with Chip8.Data;
with Chip8.Slave;

package body Chip8.Opcodes is
   use Interfaces;
   use Data;
   use Slave;
   package Random_Byte is new Ada.Numerics.Discrete_Random(Byte);

   Chip8_Function_Table : Opcode_Table_Type;
   Generator : Random_Byte.Generator;
   Carry_Register : constant Register_Index_Type := V'Last;

   procedure Chip8_Instruction_0xxx is
      Instruction : Short := Opcode and 16#00FF#;
   begin
      case Instruction is
         when 16#00E0# =>
            Clear_Display;
            PC_Increment(2);
         when 16#00EE# =>
            Stack_Pop;
         when others   => --RCA 1802 not implemented
            Chip8_Instruction_Unimplemented;
      end case;
   end Chip8_Instruction_0xxx;

   procedure Chip8_Instruction_1xxx is
      Address : Memory_Index_Type := Opcode and 16#0FFF#;
   begin
      Set_PC(Address);
   end Chip8_Instruction_1xxx;

   procedure Chip8_Instruction_2xxx is
      Address : Memory_Index_Type := Opcode and 16#0FFF#;
   begin
      Stack_Push;
      Set_PC(Address);
   end Chip8_Instruction_2xxx;

   procedure Chip8_Instruction_3xxx is
      X  : Register_Index_Type := Shift_Right(Opcode and 16#0F00#, 8);
      NN : Byte                := Opcode and 16#00FF#;
   begin
      if V(X) = NN then
         PC_Increment(4);
      else
         PC_Increment(2);
      end if;
   end Chip8_Instruction_3xxx;

   procedure Chip8_Instruction_4xxx is
      X  : Register_Index_Type := Shift_Right(Opcode and 16#0F00#, 8);
      NN : Byte                := Opcode and 16#00FF#;
   begin
      if V(X) /= NN then
         PC_Increment(4);
      else
         PC_Increment(2);
      end if;
   end Chip8_Instruction_4xxx;

   procedure Chip8_Instruction_5xxx is
      X : Register_Index_Type := Shift_Right(Opcode and 16#0F00#, 8);
      Y : Register_Index_Type := Shift_Right(Opcode and 16#00F0#, 4);
   begin
      if V(X) = V(Y) then
         PC_Increment(4);
      else
         PC_Increment(2);
      end if;
   end Chip8_Instruction_5xxx;

   procedure Chip8_Instruction_6xxx is
      X  : Register_Index_Type := Shift_Right(Opcode and 16#0F00#, 8);
      NN : Byte                := Opcode and 16#00FF#;
   begin
      V(X) := NN;
      PC_Increment(2);
   end Chip8_Instruction_6xxx;

   procedure Chip8_Instruction_7xxx is
      X  : Register_Index_Type := Shift_Right(Opcode and 16#0F00#, 8);
      NN : Byte                := Opcode and 16#00FF#;
   begin
      V(X) := V(X) + NN;
      PC_Increment(2);
   end Chip8_Instruction_7xxx;

   procedure Chip8_Instruction_8xxx is
      Instruction : Short               := Opcode and 16#000F#;
      X           : Register_Index_Type := Shift_Right(Opcode and 16#0F00#, 8);
      Y           : Register_Index_Type := Shift_Right(Opcode and 16#00F0#, 4);
   begin
      case Instruction is
         when 16#0000# =>
            V(X) := V(Y);
         when 16#0001# =>
            V(X) := V(X) or V(Y);
         when 16#0002# =>
            V(X) := V(X) and V(Y);
         when 16#0003# =>
            V(X) := V(X) xor V(Y);
         when 16#0004# =>
            if V(Y) > (255 - V(X)) then
               V(Carry_Register) := 1;
            else
               V(Carry_Register) := 0;
            end if;
            V(X) := V(X) + V(Y);
         when 16#0005# =>
            if V(Y) > V(X) then
               V(Carry_Register) := 0;
            else
               V(Carry_Register) := 1;
            end if;
            V(X) := V(X) - V(Y);
         when 16#0006# =>
            V(Carry_Register) := V(X) and 1;
            V(X) := Shift_Right(V(X), 1);
         when 16#0007# =>
            if V(X) > V(Y) then
               V(Carry_Register) := 0;
            else
               V(Carry_Register) := 1;
            end if;
            V(X) := V(Y) - V(X);
         when 16#000E# =>
            V(Carry_Register) := Shift_Right(V(X), 7);
            V(X) := Shift_Left(V(X), 1);
         when others =>
            Chip8_Instruction_Unimplemented;
      end case;
      PC_Increment(2);
   end Chip8_Instruction_8xxx;

   procedure Chip8_Instruction_9xxx is
      X : Register_Index_Type := Shift_Right(Opcode and 16#0F00#, 8);
      Y : Register_Index_Type := Shift_Right(Opcode and 16#00F0#, 4);
   begin
      if V(X) /= V(Y) then
         PC_Increment(4);
      else
         PC_Increment(2);
      end if;
   end Chip8_Instruction_9xxx;

   procedure Chip8_Instruction_Axxx is
      Address : Short := Opcode and 16#0FFF#;
   begin
      I := Address;
      PC_Increment(2);
   end Chip8_Instruction_Axxx;

   procedure Chip8_Instruction_Bxxx is
      Address : Memory_Index_Type := Opcode and 16#0FFF#;
      V0      : Memory_Index_Type := Memory_Index_Type(V(0));
   begin
      Set_PC(Address + V0);
   end Chip8_Instruction_Bxxx;

   procedure Chip8_Instruction_Cxxx is
      X  : Register_Index_Type := Shift_Right(Opcode and 16#0F00#, 8);
      NN : Byte                := Opcode and 16#00FF#;
   begin
      V(X) := NN and Random_Byte.Random(Generator);
      PC_Increment(2);
   end Chip8_Instruction_Cxxx;

   procedure Chip8_Instruction_Dxxx is
      X            : Register_Index_Type := Shift_Right(Opcode and 16#0F00#, 8);
      Y            : Register_Index_Type := Shift_Right(Opcode and 16#00F0#, 4);
      Height       : Natural             := (Opcode and 16#000F#) - 1;
      Pixel        : Byte                := 0;
   begin
      V(Carry_Register) := 0;

      if Natural(I) + Height > Memory_Index_Type'Last then
         Chip8_Error;
      end if;

      for Y_Plane in Natural range 0 .. Height loop
         Pixel := Memory(I + Y_Plane);
         for X_Plane in Natural range 0 .. 7 loop
            declare
               Point_X : Screen_Width  := (V(X) + X_Plane) mod (Screen_Width'Last  + 1);
               Point_Y : Screen_Height := (V(Y) + Y_Plane) mod (Screen_Height'Last + 1);
            begin
               if (Pixel and Shift_Right(16#80#, X_Plane)) /= 0 then
                  if Display_Area(Point_X, Point_Y) = True then
                     V(Carry_Register) := 1;
                  end if;
                  Display_Area(Point_X, Point_Y) := Display_Area(Point_X, Point_Y) xor True;
               end if;
            end;
         end loop;
      end loop;
      PC_Increment(2);
   end Chip8_Instruction_Dxxx;

   procedure Chip8_Instruction_Exxx is
      X           : Register_Index_Type := Shift_Right(Opcode and 16#0F00#, 8);
      Instruction : Short               := Opcode and 16#00FF#;
   begin
      case Instruction is
         when 16#009E# =>
            if Get_Key(V(X)) = True then
               PC_Increment(4);
            else
               PC_Increment(2);
            end if;
         when 16#00A1# =>
            if Get_Key(V(X)) = False then
               PC_Increment(4);
            else
               PC_Increment(2);
            end if;
         when others =>
            Chip8_Instruction_Unimplemented;
      end case;
   end Chip8_Instruction_Exxx;

   procedure Chip8_Instruction_Fxxx is
      X           : Register_Index_Type := Shift_Right(Opcode and 16#0F00#, 8);
      Instruction : Short               := Opcode and 16#00FF#;
   begin
      case Instruction is
         when 16#0007# =>
            V(X) := Get_Delay_Timer;
         when 16#000A# =>
            V(X) := Await_Keypress;
         when 16#0015# =>
            Set_Delay_Timer(V(X));
         when 16#0018# =>
            Set_Sound_Timer(V(X));
         when 16#001E# =>
            I := I + V(X);
         when 16#0029# =>
            I := Get_Char(V(X));
         when 16#0033# =>
            Store_BCD(V(X));
         when 16#0055# =>
            Register_Dump;
         when 16#0065# =>
            Register_Load;
         when others =>
            Chip8_Instruction_Unimplemented;
      end case;
      PC_Increment(2);
   end Chip8_Instruction_Fxxx;

   procedure Chip8_Instruction_Unimplemented is
   begin
      Ada.Text_IO.Put_Line("Unimplemented opcode");
      Chip8_Error;
   end Chip8_Instruction_Unimplemented;

   procedure Run_Instruction(Instruction: in Natural) is
   begin
      Chip8_Function_Table(Instruction).all;
   end Run_Instruction;

begin
   Chip8_Function_Table(Opcode_Table_Index_Type'Range) := (Chip8_Instruction_0xxx'Access,
                                                           Chip8_Instruction_1xxx'Access,
                                                           Chip8_Instruction_2xxx'Access,
                                                           Chip8_Instruction_3xxx'Access,
                                                           Chip8_Instruction_4xxx'Access,
                                                           Chip8_Instruction_5xxx'Access,
                                                           Chip8_Instruction_6xxx'Access,
                                                           Chip8_Instruction_7xxx'Access,
                                                           Chip8_Instruction_8xxx'Access,
                                                           Chip8_Instruction_9xxx'Access,
                                                           Chip8_Instruction_Axxx'Access,
                                                           Chip8_Instruction_Bxxx'Access,
                                                           Chip8_Instruction_Cxxx'Access,
                                                           Chip8_Instruction_Dxxx'Access,
                                                           Chip8_Instruction_Exxx'Access,
                                                           Chip8_Instruction_Fxxx'Access);
end Chip8.Opcodes;
