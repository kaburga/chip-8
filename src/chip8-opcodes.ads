private package Chip8.Opcodes is
   procedure Run_Instruction(Instruction: in Natural);

private
   type Opcode_Procedure_Type is access procedure;
   subtype Opcode_Table_Index_Type is Natural range 0 .. 15;
   type Opcode_Table_Type is array (Opcode_Table_Index_Type) of Opcode_Procedure_Type;

   procedure Chip8_Instruction_0xxx;
   procedure Chip8_Instruction_1xxx;
   procedure Chip8_Instruction_2xxx;
   procedure Chip8_Instruction_3xxx;
   procedure Chip8_Instruction_4xxx;
   procedure Chip8_Instruction_5xxx;
   procedure Chip8_Instruction_6xxx;
   procedure Chip8_Instruction_7xxx;
   procedure Chip8_Instruction_8xxx;
   procedure Chip8_Instruction_9xxx;
   procedure Chip8_Instruction_Axxx;
   procedure Chip8_Instruction_Bxxx;
   procedure Chip8_Instruction_Cxxx;
   procedure Chip8_Instruction_Dxxx;
   procedure Chip8_Instruction_Exxx;
   procedure Chip8_Instruction_Fxxx;
   procedure Chip8_Instruction_Unimplemented;

end Chip8.Opcodes;
