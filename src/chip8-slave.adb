package body Chip8.Slave is
   use type Interfaces.Unsigned_16;

   function Shift_Left(Value: Byte; Amount: Natural) return Short is
   begin
      return Short(Interfaces.Shift_Left(Short(Value), Amount));
   end Shift_Left;

   function Shift_Right(Value: Short; Amount: Natural) return Register_Index_Type is
      Result : Short := (Interfaces.Shift_Right(Value, Amount));
   begin
      if Natural(Result) > Register_Index_Type'Last then
         Chip8_Error;
         return 0;
      end if;
      return Register_Index_Type(Result);
   end Shift_Right;

   function "and" (A: Short; B: Short) return Memory_Index_Type is
      Result : Short := Interfaces."and"(A, B);
   begin
      if Natural(Result) > Memory_Index_Type'Last then
         Chip8_Error;
         return 0;
      end if;
      return Memory_Index_Type(Result);
   end "and";

   function "and" (A: Short; B: Short) return Byte is
      Result : Short := Interfaces."and"(A, B);
   begin
      if Result > Short(Byte'Last) then
         Chip8_Error;
         return 0;
      end if;
      return Byte(Result);
   end "and";

   function "and" (A: Input_Keys_Index_Type; B: Natural) return Input_Keys_Index_Type is
      Result : Byte := Interfaces."and"(Byte(A), Byte(B));
   begin
      if Natural(Result) > Input_Keys_Index_Type'Last then
         Chip8_Error;
         return 0;
      end if;
      return Input_Keys_Index_Type(Result);
   end "and";

   function "+" (A: Short; B: Byte) return Short is
   begin
      return A + Short(B);
   end "+";

   function "+" (A: Short; B: Natural) return Memory_Index_Type is
      Result : Natural := Natural(A) + B;
   begin
      if Result > Memory_Index_Type'Last then
         Chip8_Error;
         return 0;
      end if;
      return Memory_Index_Type(Result);
   end "+";

   function "+" (A: Byte; B: Natural) return Natural is
   begin
      return Natural(A) + B;
   end "+";

end Chip8.Slave;
