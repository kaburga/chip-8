
private package Chip8.Slave is
   function Shift_Left(Value: Byte; Amount: Natural) return Short;
   function Shift_Right(Value: Short; Amount: Natural) return Register_Index_Type;
   function "and" (A: Short; B: Short) return Memory_Index_Type;
   function "and" (A: Short; B: Short) return Byte;
   function "and" (A: Input_Keys_Index_Type; B: Natural) return Input_Keys_Index_Type;
   function "+" (A: Short; B: Byte) return Short;
   function "+" (A: Short; B: Natural) return Memory_Index_Type;
   function "+" (A: Byte; B: Natural) return Natural;
end Chip8.Slave;
