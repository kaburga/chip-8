with Ada.Text_IO;
with Ada.Sequential_IO;
with Ada.Integer_Text_IO;
with Ada.Directories;
with Chip8.Opcodes;
with Chip8.Data;
with Chip8.Human_Machine_Interaction;
with Chip8.Human_Machine_Interaction.Sound_Handler;
with Chip8.Slave;

package body Chip8 is
   use type Interfaces.Unsigned_16;
   use type Interfaces.Unsigned_8;
   use Data;
   use Opcodes;
   use Human_Machine_Interaction;
   use Sound_Handler;
   use Slave;

   procedure Load_Program(Filename : in String; Success: out Boolean) is
      use Ada;
      package Byte_IO is new Sequential_IO(Byte);
      Program_File        : Byte_IO.File_Type;
      In_Byte             : Byte    := 0;
      Program_Size        : Natural := 0;
      Index               : Natural := 16#200#; --512
      Program_File_Exists : Boolean := Directories.Exists(Filename);
   begin
      if not Program_File_Exists then
         Text_IO.Put_Line("Program file " & Filename & " could not be found.");
         Success := False;
         return;
      end if;

      Program_Size := Natural(Directories.Size(Filename));
      if Program_Size > (Memory'Length - 16#200#) then
         Text_IO.Put_Line("Out of memory. Program file " & Filename & " is too big.");
         Success := False;
         return;
      end if;

      Byte_IO.Open(File => Program_File,
                   Mode => Byte_IO.In_File,
                   Name => Filename);

      while not Byte_IO.End_Of_File(Program_File) loop
         Byte_IO.Read(Program_File, In_Byte);
         Memory(Index) := In_Byte;
         Index := Index + 1;
      end loop;

      Byte_IO.Close(Program_File);
      Success := True;
      Halt := False;
      return;
   end Load_Program;

   procedure Run is
      --  480/60 = 8
      Number_Of_Opcodes_Per_Frame  : constant Natural := Number_Of_Opcodes_Per_Second/Frames_Per_Second; 
   begin
      Display_Init;
      Sound_Init;
      loop
         for I in 1 .. Number_Of_Opcodes_Per_Frame loop
            if Halt = True then
               Ada.Text_IO.Put_Line("Execution halted.");
               Display_Close;
               Sound_Exit;
               return;
            end if;
            Fetch_Opcode;
            Execute_Opcode;
            Input_Keys_Status_Update;
         end loop;
         Update_Timers;
         Display_Update;
      end loop;
   end Run;

   procedure Fetch_Opcode is
      Index  : Memory_Index_Type := PC;
      Byte_1 : Short             := Shift_Left(Memory(Index), 8);
      Byte_2 : Short             := Short(Memory(Index+1));
   begin
      Opcode := Byte_1 or Byte_2;
   end Fetch_Opcode;

   procedure Execute_Opcode is
      Instruction : Natural := Shift_Right(Opcode, 12);
   begin
      Run_Instruction(Instruction);
   end Execute_Opcode;

   procedure Register_Dump is
      X : Natural := Shift_Right(Opcode and 16#0F00#, 8);
   begin
      if Natural(I) + X > Memory_Index_Type'Last then
         Chip8_Error;
      end if;
      for Register_Index in Natural range 0 .. X loop
         Memory(I+Register_Index) := V(Register_Index);
      end loop;
   end Register_Dump;

   procedure Register_Load is
      X : Natural := Shift_Right(Opcode and 16#0F00#, 8);
   begin
      if Natural(I) > Memory_Index_Type'Last then
         Chip8_Error;
      elsif Natural(I) + X > Memory_Index_Type'Last then
         Chip8_Error;
      end if;
      for Register_Index in Natural range 0 .. X loop
         V(Register_Index) := Memory(I+Register_Index);
      end loop;
   end Register_Load;

   procedure Set_PC(N: in Memory_Index_Type) is
   begin
      PC := N;
   end Set_PC;

   procedure Stack_Pop is
   begin
      if (SP - 1) < Register_Index_Type'First then
         Chip8_Error;
      end if;
      SP := SP - 1;
      PC := Stack(SP) + 2;
   end Stack_Pop;

   procedure Stack_Push is
   begin
      Stack(SP) := Short(PC);
      if (SP + 1) > Register_Index_Type'Last then
         Chip8_Error;
      end if;
      SP := SP + 1;
   end Stack_Push;

   function Get_Char(N : in Byte) return Short is
   begin
      return Short(N) * 5;
   end Get_Char;

   procedure PC_Increment(N: in Memory_Index_Type) is
   begin
      if (PC + N) > Memory_Index_Type'Last then
         Chip8_Error;
      end if;
      PC := PC + N;
   end PC_Increment;

   procedure Update_Timers is
   begin
      if Delay_Timer > 0 then
         Delay_Timer := Delay_Timer - 1;
      end if;
      if Sound_Timer = 1 then
         Play_Soundeffect;
         Sound_Timer := 0;
      elsif Sound_Timer > 0 then
         Sound_Timer := Sound_Timer - 1;
      end if;
   end Update_Timers;

   function Get_Delay_Timer return Byte is
   begin
      return Delay_Timer;
   end Get_Delay_Timer;

   procedure Set_Sound_Timer(Time : in Byte) is
   begin
      if Time > 0 then
         Sound_Timer := Time;
      end if;
   end Set_Sound_Timer;

   procedure Set_Delay_Timer(Time : in Byte) is
   begin
      if Time > 0 then
         Delay_Timer := Time;
      end if;
   end Set_Delay_Timer;

   function Get_Key(Key : in Byte) return Boolean is
      Key_Index : Input_Keys_Index_Type := Input_Keys_Index_Type(Key) and 16#0F#;
   begin
      return Input_Keys(Key_Index);
   end Get_Key;

   procedure Clear_Display is
   begin
      Display_Area := (others =>(others => False));
   end Clear_Display;

   function Await_Keypress return Byte is
   begin
      Input_Keys_Status_Update;
      for Key_Index in Input_Keys'Range loop
         if Input_Keys(Key_Index) = True then
            return Byte(Key_Index);
         end if;
      end loop;
      if Halt = True then
         return 0;
      else
         Set_PC(PC-2);
         return 0;
      end if;
   end Await_Keypress;

   procedure Chip8_Error is
   begin
      Ada.Text_IO.Put_Line("ERROR");
      Halt := True;
   end Chip8_Error;

   procedure Store_BCD(Value : in Byte) is
      Index : Memory_Index_Type := Memory_Index_Type(I);
   begin
      Memory(Index)   := Value / 100;
      Memory(Index+1) := (Value / 10) mod 10;
      Memory(Index+2) := Value mod 10;
   end Store_BCD;

   procedure Set_Key(Key: in Natural; Value: in Boolean) is
   begin
      Input_Keys(Key) := Value;
   end Set_Key;

end Chip8;
