with Interfaces;

package Chip8 is
   procedure Load_Program(Filename: in String; Success : out Boolean);
   procedure Run;

private
   subtype Byte is Interfaces.Unsigned_8;
   subtype Short is Interfaces.Unsigned_16;

   subtype Memory_Index_Type is Natural range 0 .. 4095;
   type Memory_Type is array (Memory_Index_Type) of Byte;

   subtype Stack_Index_Type is Natural range 0 .. 15;
   type Stack_Type is array (Stack_Index_Type) of Short;

   subtype Register_Index_Type is Natural range 0 .. 15;
   type Register_Type is array (Register_Index_Type) of Byte;

   subtype Pixel_Type is Boolean;
   subtype Screen_Width is Natural range 0 .. 63;
   subtype Screen_Height is Natural range 0 .. 31;
   type Display_Type is array (Screen_Width, Screen_Height) of Pixel_Type;

   subtype Input_Keys_Index_Type is Natural range 0 .. 15;
   type Input_Keys_Type is array (Input_Keys_Index_Type) of Boolean;

   procedure Fetch_Opcode;
   procedure Execute_Opcode;
   procedure Register_Dump;
   procedure Register_Load;
   procedure Clear_Display;
   procedure Stack_Pop;
   procedure Set_PC(N : in Memory_Index_Type);
   procedure Stack_Push;
   function Get_Key(Key : Byte) return Boolean;
   function Await_Keypress return Byte;
   function Get_Delay_Timer return Byte;
   procedure Set_Delay_Timer(Time : in Byte);
   procedure Set_Sound_Timer(Time : in Byte);
   function Get_Char(N : in Byte) return Short;
   procedure PC_Increment(N : in Memory_Index_Type);
   procedure Chip8_Error;
   procedure Store_BCD(Value : in Byte);
   procedure Set_Key(Key : in Natural; Value : in Boolean);
   procedure Update_Timers;
end Chip8;
