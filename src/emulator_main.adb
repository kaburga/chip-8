with Ada.Text_IO;
with Ada.Command_Line;
with Chip8;

procedure Emulator_Main is
   Input_File : String := Ada.Command_Line.Argument(1);
   Load_Program_Successful : Boolean := False;
begin
   Chip8.Load_Program(Input_File, Load_Program_Successful);
   if Load_Program_Successful = False then
      return;
   end if;
   Chip8.Run;
end Emulator_Main;

